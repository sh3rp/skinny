import org.junit.Before;
import org.junit.Test;
import org.spacerobots.skinny.server.ServerHandler;

/**
 *
 *
 *
 */
public class ServerHandlerTest {

    private ServerHandler handler;

    private static final String         TEST_STRING = "This is a test string.";

    @Before
    public void before() {
        this.handler = new ServerHandler();
    }

    @Test
    public void toByteArrayTest() {
        char[] chars = new String(TEST_STRING).toCharArray();
        byte[] bytes = handler.toByteArray(chars);
        char[] newChars = handler.toCharArray(bytes);
        String str = new String(newChars);
        assert(str.equals(TEST_STRING));
    }

}

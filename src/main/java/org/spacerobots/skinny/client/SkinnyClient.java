package org.spacerobots.skinny.client;

/**
 *
 *
 *
 */
public abstract class SkinnyClient {
    private String server;
    private int port;

    public abstract byte[] get(String key);
    public abstract void put(String key, byte[] bytes);
    public abstract void delete(String key);
}

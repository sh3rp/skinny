package org.spacerobots.skinny.server;

import org.apache.log4j.Logger;
import org.eclipse.jetty.server.Server;
import org.spacerobots.skinny.auth.BasicAuthenticator;
import org.spacerobots.skinny.db.BerkeleyDataStore;

import java.io.File;


/**
 *
 * Skinny bootstrap.
 *
 */
public class SkinnyServer {

    private static Logger LOGGER = Logger.getLogger(SkinnyServer.class);

    public static void main(String args[]) {
        String authFile = null;
        if(args.length == 1)
            authFile = args[0];
        SkinnyServer skinnyServer = new SkinnyServer(authFile);
        skinnyServer.start();
    }

    private Server jetty;
    private BerkeleyDataStore database;
    public SkinnyServer(String authFile) {
        this.jetty = new Server(8999);
        jetty.setHandler(new ServerHandler(new BasicAuthenticator(new File(authFile))));
    }

    public void start() {
        LOGGER.debug("Starting server.");
        try {
            jetty.start();
            jetty.join();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

package org.spacerobots.skinny.server;

import org.apache.log4j.Logger;
import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.server.handler.AbstractHandler;
import org.spacerobots.skinny.auth.Authenticator;
import org.spacerobots.skinny.auth.BasicAuthenticator;
import org.spacerobots.skinny.db.BerkeleyDataStore;
import org.spacerobots.skinny.db.DataStore;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * Handles all server requests into Jetty.
 *
 */
public class ServerHandler extends AbstractHandler {

    private static Logger LOGGER = Logger.getLogger(ServerHandler.class);

    private Authenticator authenticator;

    private Map<String,DataStore> datastores;

    public ServerHandler() {
        this(new BasicAuthenticator());
    }

    public ServerHandler(Authenticator auth) {
        this.authenticator = auth;
        this.datastores = new HashMap<String,DataStore>();
    }

    @Override
    public void handle(String target, Request baseRequest, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

        LOGGER.debug("Handling request.");

        if(!authenticator.authenticated(request)) {
            response.setStatus(401);
            baseRequest.setHandled(true);
            LOGGER.debug("User " + authenticator.getUsername(request) + " cannot be authenticated.");
            return;
        }

        String username = authenticator.getUsername(request);

        LOGGER.debug("Authenticated: " + username);

        byte[] key = target.getBytes("utf-8");

        if(request.getMethod().equals("GET")) {
            LOGGER.debug("Handling GET request.");
            get(key, username, request, response);
        } else if(request.getMethod().equals("PUT") || request.getMethod().equals("POST")) {
            LOGGER.debug("Handling POST/PUT request.");
            put(key, username, request, response);
        } else if(request.getMethod().equals("DELETE")) {
            LOGGER.debug("Handling DELETE request.");
            delete(key, username, response);
        }

        LOGGER.debug("Target: " + target + " (" + response.getStatus() + ")");

        baseRequest.setHandled(true);

    }

    public DataStore getDatastore(String username) {
        DataStore ds = null;
        if(datastores.containsKey(username))
            ds = datastores.get(username);
        else {
            ds = new BerkeleyDataStore(username,"db");
            datastores.put(username,ds);
        }
        return ds;
    }

    public void get(byte[] key, String username, HttpServletRequest request, HttpServletResponse response) throws IOException {
        DataStore ds = getDatastore(username);
        Map<byte[], byte[]> results = ds.search(key);
        if (results.size() == 1) {
            response.setContentType(request.getContentType());
            response.getOutputStream().write(ds.get(key));
            response.setStatus(200);
        } else if (results.size() > 1) {
            for (byte[] k : results.keySet())
                response.getOutputStream().print(new String(k) + "\n");
            response.setStatus(207);
        } else
            response.setStatus(404);
    }

    public void put(byte[] key, String username, HttpServletRequest request, HttpServletResponse response) throws IOException {
        DataStore ds = getDatastore(username);
        if (ds.get(key) == null) {
            BufferedReader r = null;
            r = request.getReader();
            int len = request.getContentLength();
            char[] buf = new char[len];
            r.read(buf);
            r.close();
            ds.put(key, toByteArray(buf));
            response.setStatus(201);
        } else
            response.setStatus(403);
    }

    public void delete(byte[] key, String username, HttpServletResponse response) {
        DataStore ds = getDatastore(username);

        if(ds.get(key) != null) {
            ds.delete(key);
            response.setStatus(202);
        } else
            response.setStatus(404);
    }

    public byte[] toByteArray(char[] chars) {
        if(chars == null)
            return new byte[0];

        byte[] bytes = new byte[chars.length*2+1];
        for(int a=0;a<chars.length;a++) {
            bytes[a*2] = (byte)(chars[a] & 0x00ff);
            bytes[a*2+1] = (byte)((chars[a] & 0xff00) >>> 8);
        }

        return bytes;
    }

    public char[] toCharArray(byte[] bytes) {
        if(bytes == null)
            return new char[0];

        char[] chars = new char[bytes.length/2];
        for(int a=0;a<chars.length;a++)
            chars[a] = (char)((bytes[a*2] & 0x00ff) | ((bytes[a*2+1] << 8) & 0xff00));

        return chars;
    }

}

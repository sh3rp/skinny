package org.spacerobots.skinny.db;

import java.util.Map;

/**
 * org.spacerobots.skinny.db
 */


public interface DataStore {
    public byte[] get(byte[] key);
    public void put(byte[] key, byte[] value);
    public void delete(byte[] key);
    public Map<byte[],byte[]> search(byte[] key);
}

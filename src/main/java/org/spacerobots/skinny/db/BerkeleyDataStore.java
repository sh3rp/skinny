package org.spacerobots.skinny.db;

import com.sleepycat.je.*;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * BDB implementation of a backing datastore.
 *
 */
public class BerkeleyDataStore implements DataStore {

    private static Logger logger = Logger.getLogger(BerkeleyDataStore.class);

    Environment myDbEnvironment;
    Database myDatabase;

    public BerkeleyDataStore(String dbName, String dbDir) {
        try {
            // Open the environment. Create it if it does not already exist.
            EnvironmentConfig envConfig = new EnvironmentConfig();
            envConfig.setAllowCreate(true);
            File dir = new File(dbDir + File.separator + dbName);
            if(!dir.exists())
                dir.mkdirs();
            if(logger.isDebugEnabled())
                logger.debug("Opening db: " + dir.getAbsolutePath());
            myDbEnvironment = new Environment(dir, envConfig);

            // Open the database. Create it if it does not already exist.
            DatabaseConfig dbConfig = new DatabaseConfig();
            dbConfig.setAllowCreate(true);
            myDatabase = myDbEnvironment.openDatabase(null,
                    dbName,
                    dbConfig);
        } catch (DatabaseException dbe) {
            logger.error(dbe.getMessage());
        }
    }

    @Override
    public byte[] get(byte[] key) {
        DatabaseEntry value = new DatabaseEntry();
        if(myDatabase.get(null, new DatabaseEntry(key),value,LockMode.DEFAULT) == OperationStatus.SUCCESS)
            return value.getData();
        else
            return null;
    }

    @Override
    public void put(byte[] key, byte[] value) {
        synchronized(myDatabase) {
            myDatabase.put(null, new DatabaseEntry(key), new DatabaseEntry(value));
        }

    }

    @Override
    public void delete(byte[] key) {
        synchronized(myDatabase) {
            myDatabase.delete(null, new DatabaseEntry(key));
        }
    }

    @Override
    public Map<byte[], byte[]> search(byte[] key) {
        if(logger.isDebugEnabled())
            logger.debug("SEARCH: " + new String(key));
        Map<byte[],byte[]> results = new HashMap<byte[], byte[]>();
        Cursor cursor = myDatabase.openCursor(null,null);
        DatabaseEntry dbKey = new DatabaseEntry(key);
        DatabaseEntry dbValue = new DatabaseEntry();
        OperationStatus status = cursor.getSearchKeyRange(dbKey, dbValue, LockMode.DEFAULT);
        if(status == OperationStatus.NOTFOUND)
            return results;
        int ctr = 0;

        byte[] lastData = new byte[1];
        while(startsWith(key,dbKey.getData()) && !Arrays.equals(dbKey.getData(),lastData)) {
            lastData = dbKey.getData();
            results.put(dbKey.getData(),dbValue.getData());
            cursor.getNext(dbKey, dbValue, LockMode.DEFAULT);
        }
        cursor.close();
        return results;
    }

    private boolean startsWith(byte[] startsWith, byte[] byteStr) {
        if(startsWith == null || byteStr == null)
            return false;
        if(startsWith.length > byteStr.length)
            return false;
        for(int a=0;a<startsWith.length;a++)
            if(byteStr[a] != startsWith[a])
                return false;
        return true;
    }
}

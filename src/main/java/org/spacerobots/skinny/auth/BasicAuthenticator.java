package org.spacerobots.skinny.auth;

import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.xml.bind.DatatypeConverter;
import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * org.spacerobots.skinny.auth
 */


public class BasicAuthenticator implements Authenticator {

    private Logger logger = Logger.getLogger(BasicAuthenticator.class);

    private File htpasswdFile;
    private BufferedReader fileReader;

    private Map<String,String> userCredentials;

    public BasicAuthenticator() {
        this(new File("conf" + File.pathSeparator + "htpasswd"));
    }

    public BasicAuthenticator(File passwdFile) {
        this.userCredentials = new HashMap<String, String>();
        setHtpasswdFile(passwdFile);
    }

    public void setHtpasswdFile(File file) {
        this.htpasswdFile = file;
        try {
            this.fileReader = new BufferedReader(new FileReader(htpasswdFile));
        } catch (FileNotFoundException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            return;
        }
        userCredentials.clear();

        String tmp;
        try {
            while((tmp = fileReader.readLine()) != null) {
                String[] userPass = tmp.split(":");
                userCredentials.put(userPass[0],userPass[1]);
            }
        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

    }

    @Override
    public boolean authenticated(HttpServletRequest request) {
        String[] creds = getCredentials(request);
        if(creds == null || creds[0] == null || creds[1] == null)
            return false;
        String username = creds[0];
        String password = creds[1];
        return userCredentials.containsKey(username) && userCredentials.get(username).equals(password);  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public String getUsername(HttpServletRequest request) {
        return getCredentials(request)[0];
    }

    @Override
    public Set<String> getUsers() {
        return userCredentials.keySet();  //To change body of implemented methods use File | Settings | File Templates.
    }

    private String[] getCredentials(HttpServletRequest request) {
        String authString = request.getHeader("Authorization");
        if(authString == null)
            return new String[]{null,null};
        String base64 = authString.substring(6);
        byte[] decoded = DatatypeConverter.parseBase64Binary(base64);
        String[] creds = new String(decoded).split(":");
        return creds;
    }
}

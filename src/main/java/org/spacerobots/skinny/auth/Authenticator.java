package org.spacerobots.skinny.auth;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Set;

/**
 * org.spacerobots.skinny.auth
 */


public interface Authenticator {
    public boolean authenticated(HttpServletRequest request);
    public String getUsername(HttpServletRequest request);
    public Set<String> getUsers();
}
